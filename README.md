# Overview

This project is to demonstrate a series of common scenarios often encountered with GitLab CICD pipelines.

# Scenarios

## Run pipeline or job on failure only

This scenario demonstrates how to run a pipeline (in same or separate project) only when a job fails

* Includes some context (variables)

### Results

While it's possible to trigger a child pipeline on failure only, there is no way to get a pipeline to continue after the success of the manual sub pipeline. Take a look at the pipeline runs and see how many permeations were run, it's just not gonna happen :-(
